# Package

version       = "0.1.0"
author        = "Sotolf"
description   = "A simple text editor"
license       = "LGPL-3.0-or-later"
srcDir        = "src"
bin           = @["vin"]


# Dependencies

requires "nim >= 1.6.12"
requires "illwill >= 0.3.0"
