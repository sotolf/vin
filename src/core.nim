import std/monotimes
import std/options
import std/strformat
import std/strutils
import std/times
import illwill
import gapBuffer
import unicode

type
  EditorMode* = enum
    Normal
    Insert
    Command
    Visual

type 
  EditorState* = object
    lineFrom*: int
    lineTo*: int
    colFrom*: int
    colTo*: int
    mode*: EditorMode
    filePath*: string
    commandBuffer*: GapBuffer
    exit*: bool
    message*: string
    messageTime*: Option[Duration]
    timestamp*: Option[MonoTime]
    keybuffer*: seq[Rune]

proc initState*(filepath: string = ""): EditorState =
  EditorState(
    lineFrom: 0,
    lineTo: 0,
    colFrom: 0,
    colTo: 0,
    mode: EditorMode.Normal,
    filePath: filepath,
    commandBuffer: newGapBuffer(),
    exit: false,
    message: "",
    messageTime: none(Duration),
    timestamp: none(MonoTime),
    keybuffer: @[],
  )

proc setMessage(state: var EditorState, message: string, seconds: int = 2) =
  state.message = message
  state.messageTime = some initDuration(seconds = seconds)

proc saveFile(state: var EditorState, buffer: GapBuffer) =
  if state.filePath == "":
    state.setMessage("No filename")
  else:
    var content = $buffer
    if not content.endsWith("\n"):
      content &= "\n"
    try:
      writeFile(state.filePath, content)
    except IOError as err:
      state.setMessage(err.msg)

proc executeCommand(state: var EditorState, buffer: var GapBuffer) =
  let command = $state.commandBuffer
  if command == "quit" or command == "q":
    state.exit = true
    return
  if command == "wq":
    saveFile(state, buffer)
    state.exit = true
  if command == "write" or command == "w":
    saveFile(state, buffer)
    state.setMessage(fmt"saved: {state.filepath}")
    return
  if command.startsWith("w "):
    let parts = command.split(" ")
    state.filePath = parts[1]
    saveFile(state, buffer)
    state.setMessage(fmt"saved: {state.filepath}")
    return

proc normalModeMapping(state: var Editorstate, buffer: var GapBuffer, key: Key) =
  if state.keybuffer.len == 0:
    case key
    of Key.Right, Key.L:
      buffer.charForward()
    of Key.Left, Key.H:
      buffer.charBackward()
    of Key.Down, Key.J:
      buffer.lineDown()
    of Key.Up, Key.K:
      buffer.lineUp()
    of Key.I: 
      state.mode = EditorMode.Insert
    of Key.A: 
      buffer.charForward()
      state.mode = EditorMode.Insert
    of Key.X:
      buffer.deleteForward()
    of Key.C:
      state.keybuffer.add('c'.Rune)
    of Key.ShiftC:
      buffer.delete(buffer.lineEndTextObject)
      state.mode = EditorMode.Insert
    of Key.D:
      state.keybuffer.add('d'.Rune)
    of Key.ShiftD:
      buffer.delete(buffer.lineEndTextObject)
    of Key.R:
      state.keybuffer.add('r'.Rune)
    of Key.F:
      state.keybuffer.add('f'.Rune)
    of Key.ShiftF:
      state.keybuffer.add('F'.Rune)
    of Key.T:
      state.keybuffer.add('t'.Rune)
    of Key.ShiftT:
      state.keybuffer.add('T'.Rune)
    of Key.W:
      buffer.wordForward()
    of Key.B:
      buffer.wordBackward()
    of Key.Colon:
      state.mode = EditorMode.Command
    of Key.Zero:
      buffer.gotoLineStart()
    of Key.Dollar:
      buffer.gotoLineEnd()
    of Key.ShiftI:
      buffer.gotoLineStart()
      state.mode = EditorMode.Insert
    of Key.ShiftA:
      buffer.gotoLineEnd()
      state.mode = EditorMode.Insert
    of Key.ShiftV:
      buffer.selectLine()
    else: discard
  elif state.keybuffer == "f".toRunes:
    if key == Key.None: return
    elif key == Key.Escape: 
      state.keybuffer = @[]
      return
    buffer.goto(key.ord.char.Rune)
    state.keybuffer = @[]
  elif state.keybuffer == "F".toRunes:
    if key == Key.None: return
    elif key == Key.Escape: 
      state.keybuffer = @[]
      return
    buffer.gotoBack(key.ord.char.Rune)
    state.keybuffer = @[]
  elif state.keybuffer == "t".toRunes:
    if key == Key.None: return
    elif key == Key.Escape: 
      state.keybuffer = @[]
      return
    buffer.goto(key.ord.char.Rune)
    buffer.charBackward()
    state.keybuffer = @[]
  elif state.keybuffer == "T".toRunes:
    if key == Key.None: return
    elif key == Key.Escape: 
      state.keybuffer = @[]
      return
    buffer.gotoBack(key.ord.char.Rune)
    buffer.charForward()
    state.keybuffer = @[]
  elif state.keybuffer == "r".toRunes:
    if key == Key.None: return
    elif key == Key.Escape: 
      state.keybuffer = @[]
      return
    buffer.deleteForward
    buffer.insert(key.ord.char.Rune)
    state.keybuffer = @[]
  elif state.keybuffer == "d".toRunes:
    case key
    of Key.None: return
    of Key.Escape:
      state.keybuffer = @[]
    of Key.I:
      state.keybuffer.add('i'.Rune)
    of Key.A:
      state.keybuffer.add('a'.Rune)
    of Key.F:
      state.keybuffer.add('f'.Rune)
    of Key.ShiftF:
      state.keybuffer.add('F'.Rune)
    of Key.T:
      state.keybuffer.add('t'.Rune)
    of Key.ShiftT:
      state.keybuffer.add('T'.Rune)
    of Key.W:
      buffer.delete buffer.wordForwardTextObject
      state.keybuffer = @[]
    of Key.B:
      buffer.delete buffer.wordBackwardTextObject
      state.keybuffer = @[]
    of Key.Dollar:
      buffer.delete buffer.lineEndTextObject
      state.keybuffer = @[]
    of Key.L:
      buffer.delete buffer.charForwardTextObject
      state.keybuffer = @[]
    of Key.H:
      buffer.delete buffer.charBackwardTextObject
      state.keybuffer = @[]
    else:
      state.keybuffer = @[]
  elif state.keybuffer == "di".toRunes:
    case key
    of Key.None: return
    of Key.Escape:
      state.keybuffer = @[]
    of Key.W:
      buffer.delete buffer.innerWordTextObject
      state.keybuffer = @[]
    else:
      state.keybuffer = @[]
  elif state.keybuffer == "da".toRunes:
    case key
    of Key.None: return
    of Key.Escape:
      state.keybuffer = @[]
    of Key.W:
      buffer.delete buffer.aroundWordTextObject
      state.keybuffer = @[]
    else:
      state.keybuffer = @[]
  elif state.keybuffer == "df".toRunes:
    case key
    of Key.None: return
    of Key.Escape:
      state.keybuffer = @[]
    else:
      var goto = buffer.gotoTextObject(key.ord.char.Rune)
      if goto.to > 0:
        inc goto.to
      buffer.delete goto
      state.keybuffer = @[]
  elif state.keybuffer == "dF".toRunes:
    case key
    of Key.None: return
    of Key.Escape:
      state.keybuffer = @[]
    else:
      var goto =  buffer.gotoBackTextObject(key.ord.char.Rune)
      if goto.frm < 0:
        dec goto.frm
      buffer.delete
      state.keybuffer = @[]
  elif state.keybuffer == "dt".toRunes:
    case key
    of Key.None: return
    of Key.Escape:
      state.keybuffer = @[]
    else:
      let goto = buffer.gotoTextObject(key.ord.char.Rune)
      buffer.delete goto
      state.keybuffer = @[]
  elif state.keybuffer == "dT".toRunes:
    case key
    of Key.None: return
    of Key.Escape:
      state.keybuffer = @[]
    else:
      let goto = buffer.gotoBackTextObject(key.ord.char.Rune)
      buffer.delete goto
      state.keybuffer = @[]
  elif state.keybuffer == "c".toRunes:
    case key
    of Key.None: return
    of Key.Escape:
      state.keybuffer = @[]
    of Key.I:
      state.keybuffer.add('i'.Rune)
    of Key.A:
      state.keybuffer.add('a'.Rune)
    of Key.F:
      state.keybuffer.add('f'.Rune)
    of Key.ShiftF:
      state.keybuffer.add('F'.Rune)
    of Key.T:
      state.keybuffer.add('t'.Rune)
    of Key.ShiftT:
      state.keybuffer.add('T'.Rune)
    of Key.W:
      buffer.delete buffer.wordForwardTextObject
      state.mode = EditorMode.Insert
      state.keybuffer = @[]
    of Key.B:
      buffer.delete buffer.wordBackwardTextObject
      state.mode = EditorMode.Insert
      state.keybuffer = @[]
    of Key.Dollar:
      buffer.delete buffer.lineEndTextObject
      state.mode = EditorMode.Insert
      state.keybuffer = @[]
    of Key.L:
      buffer.delete buffer.charForwardTextObject
      state.mode = EditorMode.Insert
      state.keybuffer = @[]
    of Key.H:
      buffer.delete buffer.charBackwardTextObject
      state.mode = EditorMode.Insert
      state.keybuffer = @[]
    else:
      state.keybuffer = @[]
  elif state.keybuffer == "ci".toRunes:
    case key
    of Key.None: return
    of Key.Escape:
      state.keybuffer = @[]
    of Key.W:
      buffer.delete buffer.innerWordTextObject
      state.mode = EditorMode.Insert
      state.keybuffer = @[]
    else:
      state.keybuffer = @[]
  elif state.keybuffer == "ca".toRunes:
    case key
    of Key.Escape:
      state.keybuffer = @[]
    of Key.None: return
    of Key.W:
      buffer.delete buffer.aroundWordTextObject
      state.mode = EditorMode.Insert
      state.keybuffer = @[]
    else:
      state.keybuffer = @[]
  elif state.keybuffer == "cf".toRunes:
    case key
    of Key.None: return
    of Key.Escape:
      state.keybuffer = @[]
    else:
      var goto = buffer.gotoTextObject(key.ord.char.Rune)
      if goto.to > 0:
        inc goto.to
      buffer.delete goto
      state.mode = EditorMode.Insert
      state.keybuffer = @[]
  elif state.keybuffer == "cF".toRunes:
    case key
    of Key.None: return
    of Key.Escape:
      state.keybuffer = @[]
    else:
      var goto = buffer.gotoBackTextObject(key.ord.char.Rune)
      if goto.frm < 0:
        dec goto.frm
      buffer.delete goto
      state.mode = EditorMode.Insert
      state.keybuffer = @[]
  elif state.keybuffer == "ct".toRunes:
    case key
    of Key.None: return
    of Key.Escape:
      state.keybuffer = @[]
    else:
      let goto = buffer.gotoTextObject(key.ord.char.Rune)
      buffer.delete goto
      state.mode = EditorMode.Insert
      state.keybuffer = @[]
  elif state.keybuffer == "cT".toRunes:
    case key
    of Key.None: return
    of Key.Escape:
      state.keybuffer = @[]
    else:
      let goto = buffer.gotoBackTextObject(key.ord.char.Rune)
      buffer.delete goto
      state.mode = EditorMode.Insert
      state.keybuffer = @[]

proc insertModeMapping(state: var Editorstate, buffer: var GapBuffer, key: Key) =
  case key
  of Key.Escape:
    state.mode = EditorMode.Normal
  of Key.None:
    discard
  of Key.Enter:
    buffer.insert('\n'.Rune)
  of Key.Backspace:
    buffer.delete
  of Key.Right:
    buffer.charForward()
  of Key.Left:
    buffer.charBackward()
  of Key.Down:
    buffer.lineDown()
  of Key.Up:
    buffer.lineUp()
  else:
    buffer.insert(key.ord.char.Rune)

proc commandModeMapping(state: var Editorstate, buffer: var GapBuffer, key: Key) =
  case key
  of Key.Escape:
    state.commandBuffer = newGapBuffer()
    state.mode = EditorMode.Normal
  of Key.Enter: 
    executeCommand(state, buffer)
    state.commandBuffer = newGapBuffer()
    state.mode = EditorMode.Normal
  of Key.Right:
    state.commandBuffer.charForward()
  of Key.Left:
    state.commandBuffer.charBackward()
  of Key.Backspace:
    state.commandBuffer.delete()
  of Key.None: discard
  else:
    state.commandBuffer.insert(key.ord.char.Rune)

proc handleKey*(state: var EditorState, buffer: var GapBuffer, key: Key) =
    case state.mode:
      of EditorMode.Normal:
        normalModeMapping(state, buffer, key)
      of EditorMode.Insert:
        insertModeMapping(state, buffer, key)
      of EditorMode.Command:
        commandModeMapping(state, buffer, key)
      else:
        echo "Not Implemented"
        quit(1)
