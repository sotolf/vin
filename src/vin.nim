import std/monotimes
import std/times
import std/options
import std/os
import std/strformat
import std/strutils
import illwill
import core
import gapBuffer

proc exitProc() {.noconv.} =
  illwillDeinit()
  showCursor()
  quit(0)

proc updateSize(self: var EditorState, tb: TerminalBuffer) =
  let curHeight = self.lineTo - self.lineFrom
  let curWidth = self.colTo - self.colFrom
  if tb.height - 2 > curHeight:
    self.lineTo += (tb.height - 2) - curHeight
  if tb.height - 2 < curHeight:
    self.lineTo -= curHeight - tb.height - 2
  if tb.width > curWidth:
    self.colTo += tb.width - curWidth
  if tb.width < curWidth:
    self.colTo -= curWidth - tb.width

proc adjustToCursor(self: var EditorState, buffer: GapBuffer) =
  let 
    cursorLine = buffer.cursor.line
    cursorCol = buffer.cursor.col

  # Adjust vertical
  if cursorLine >= self.lineTo - 1:
    let adjustment = cursorLine - self.lineTo + 1
    self.lineFrom += adjustment
    self.lineTo += adjustment
  elif cursorLine < self.lineFrom:
    let adjustment = self.lineFrom - cursorLine
    self.lineFrom -= adjustment
    self.lineTo -= adjustment

  # Adjust horisontal
  if cursorCol > self.colTo - 1:
    let adjustment = cursorCol - self.colTo + 1
    self.colFrom += adjustment
    self.colTo += adjustment
  elif cursorCol < self.colFrom:
    let adjustment = self.colFrom - cursorCol
    self.colFrom -= adjustment
    self.colTo -= adjustment

proc toScreenCursor(self: Cursor, state: EditorState): Cursor =
  Cursor(
    line: self.line - state.lineFrom,
    col: self.col - state.colFrom
  )

proc onScreen(self:Cursor, state: EditorState): bool =
  if self.line < state.lineFrom or self.line > state.lineTo:
    return false
  if self.col < state.colFrom or self.col > state.colTo:
    return false
  return true

proc render(tb: var TerminalBuffer, buffer: GapBuffer, state: var EditorState) =
  
  state.updateSize(tb)
  state.adjustToCursor(buffer)
  var screenLine = 0
  var idx = 0
  

  for line in buffer.lines:
    if idx >= state.lineFrom and idx <= state.lineTo:
      if line.len > state.colFrom:
        tb.write(0, screenLine, line[state.colFrom..^1])
      else:
        tb.write(0, screenLine, line)
      inc screenLine
    inc idx

  if state.messageTime.isSome:
    # display the message
    tb.write(0, tb.height - 1, state.message)

    # check how long the message has been shown
    # and reduce the time left
    if state.timestamp.isNone:
      state.timestamp = some(getMonoTime())
    else:
      let last = state.timestamp.get
      let now = getMonoTime()
      let sinceLast = now - last
      state.timestamp = some now
      state.messageTime = some(state.messageTime.get - sinceLast)
      if state.messageTime.get.inSeconds <= 0:
        state.messageTime = none Duration
        state.message = ""
        state.timestamp = none MonoTime
        
  elif state.mode == EditorMode.Command:
    tb.write(0, tb.height - 1, fmt":{$state.commandBuffer}")
    
    # draw the cursor in the minibuffer
    let cursor = state.commandBuffer.cursor
    var c = tb[cursor.col + 1, tb.height - 1]
    c.fg = fgBlack
    c.bg = bgWhite
    c.style = {styleBright}
    tb[cursor.col + 1, tb.height - 1] = c
  else:
    tb.write(0,tb.height - 1, fmt"-- {state.mode} --")
    # Calculate offset for cursor notation
    let cursorString = fmt"{buffer.cursor.line}:{buffer.cursor.col}"
    var offset = tb.width - cursorString.len
    tb.write(offset, tb.height - 1, cursorString)

    offset -= 3
    let keybuf = state.keybuffer.join()
    offset -= len keybuf
    tb.write(offset, tb.height - 1, keybuf)

    # First draw selection, so that the cursor
    # will be drawn on top of it if they overlap
    if not (buffer.selection.frm == 0 and buffer.selection.to == 0):
      for cursor in buffer.selected:
        if cursor.onScreen state:
          let cur = cursor.toScreenCursor state
          var c = tb[cur.col, cur.line]
          c.fg = fgBlack
          c.bg = bgBlue
          tb[cur.col, cur.line] = c

    # Draw cursor (inverting fg and bg)
    # Adjust the cursor to fit on screen
    let cursor = buffer.cursor.toScreenCursor(state)
    var c = tb[cursor.col, cursor.line]
    c.fg = fgBlack
    c.bg = bgWhite
    c.style = {styleBright}
    tb[cursor.col, cursor.line] = c
  

  tb.display()


proc main() =


  # Replace with some real commandline
  # parsing when we need something more
  var filePath: string
  var buffer =
    if paramCount() > 0:
      filePath = commandLineParams()[0]

      if not fileExists(filePath):
        newGapBuffer()
      else:
        readfile(filePath).toGapBuffer()
    else:
      newGapBuffer()

  illwillInit(fullscreen=true)
  #setControlCHook(exitProc)
  hideCursor()
  var state = initState(filePath)

  while true:
    var tb = newTerminalBuffer(terminalWidth(), terminalHeight())

    var key = getKey()

    handleKey(state, buffer, key)

    tb.render(buffer, state)
    
    if state.exit:
      exitProc()

    sleep 20

main()
