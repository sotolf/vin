import std/sequtils
import std/strutils
import std/options
import std/unicode
import std/sugar

type 
  Cursor* = object
    line*: int
    col*: int

type
  TextObject* = object
    frm*: int
    to*: int

type
  Selection* = object
    frm*: int
    to*: int

type
  GapBuffer* = object
    left*: seq[Rune]
    right*: seq[Rune]
    cursor*: Cursor
    selection*: Selection

func `$`*(self: GapBuffer): string =
  self.left.join & self.right.join

func `[]`*[T: Ordinal](self: GapBuffer, idx: T): Rune =
  # we don't need to care about the exception throwing since the 
  # underlying seqs will deal with that for us
  if idx < self.left.len:
    self.left[idx]
  else:
    self.right[idx - self.left.len]

func `[]`*[T,U: Ordinal](self: GapBuffer, x: HSlice[T,U]): seq[Rune] =
  for i in x.a..x.b:
    result.add self[i]

func newGapBuffer*(): GapBuffer =
  GapBuffer(left: @[], 
            right: @[],
            cursor: Cursor(line: 0, col: 0),
            selection: Selection(frm: 0, to: 0))

func toGapBuffer*(frm: string): GapBuffer =
  GapBuffer(left: @[], right: frm.toRunes, cursor: Cursor(line: 0, col: 0))

func idxToCursor*(self: GapBuffer, idx: int): Cursor = 
  let before = self[0..<idx]
  let line = before.count('\n'.Rune)
  var col = 0
  for i in countdown(before.high, 0):
    if before[i] != '\n'.Rune:
      col += 1
    else: break
  return Cursor(line: line, col: col)

func search(self: GapBuffer, needle: Rune, nth: int = 1): Option[int] =
  var found = 0
  for idx, ch in self.right:
    if ch == needle:
      result = some(idx)
      found += 1
      if found == nth:
        return
  return none(int)

func searchBack(self: GapBuffer, needle: Rune, nth: int = 1): Option[int] =
  var found = 0
  for idx in countdown(self.left.high, 0):
    if self.left[idx] == needle:
      result = some(self.left.len - idx)
      inc(found)
      if found == nth:
        return
  return none(int)

proc moveRight(self: var GapBuffer, amount: int) =
  if self.right.len < amount: return
  for ch in self.right[0..<amount]:
    if ch == '\n'.Rune:
      inc self.cursor.line
      self.cursor.col = 0
    else:
      inc self.cursor.col
  self.left &= self.right[0..<amount]
  self.right = self.right[amount..<self.right.len]

proc moveLeft(self: var GapBuffer, amount: int) =
  if self.left.len < amount:
    self.right = self.left & self.right
    self.left = @[]
    self.cursor.line = 0
    self.cursor.col = 0
    return
  # Adjust the lines before moving since we can count the
  # number of linebreaks
  var lineBreaks = 0
  for idx in countdown(self.left.high, self.left.len - amount):
    if self.left[idx] == '\n'.Rune:
      dec self.cursor.line
      inc lineBreaks

  self.right = self.left[self.left.len - amount..self.left.high] & self.right
  self.left  = self.left[0..self.left.high - amount]

  # Adjust the columns afterwards, since we then just have to count
  # how many characters there are backwards until we hit a linebreak
  self.cursor.col = 0
  for idx in countdown(self.left.high, 0):
    if self.left[idx] == '\n'.Rune:
      return
    else:
      inc self.cursor.col

proc move*(self: var GapBuffer, amount: int) =
  if amount == 0: return
  if amount > 0:
    self.moveRight(amount)
  else:
    self.moveLeft(amount.abs)

proc peek*(self: GapBuffer, count: int = 0): Option[Rune] =
  if self.right.len > count:
    some(self.right[count])
  else:
    none(Rune)

proc peekBack*(self: GapBuffer, count: int = 0): Option[Rune] =
  if self.left.len > count:
    some(self.left[self.left.high - count])
  else:
    none(Rune)

proc charForwardTextObject*(self: GapBuffer): TextObject =
  # first make sure you can actually move
  let nextOpt = self.peek()
  if nextOpt.isNone:
    return
  let next = nextOpt.get()
  if next == '\n'.Rune:
    return
  result.to = 1

proc charForward*(self: var GapBuffer) =
  let textObj = self.charForwardTextObject()
  self.move(textObj.to)

proc charBackwardTextObject*(self: GapBuffer): TextObject =
  # Make sure we can move
  let nextOpt = self.peekBack()
  if nextOpt.isNone:
    return
  let next = nextOpt.get()
  if next == '\n'.Rune:
    return
  result.to = -1

proc charBackward*(self: var GapBuffer) =
  let textObj = self.charBackwardTextObject()
  self.move(textObj.to)

func find(self: GapBuffer, needle: Rune, nth: int = 1): Option[int] =
  var found = 0
  for idx, ch in self.right:
    if ch == needle and idx != 0:
      result = some idx
      found += 1
      if found == nth:
        return
    elif ch == '\n'.Rune:
      return none int

func isAlphaNum(self: Rune): bool =
  self.isAlpha or "1234567890".toRunes.contains(self)

func findNonAlpha(self: GapBuffer): Option[int] =
  for idx, ch in self.right:
    if not ch.isAlpha() and
       not "1234567890".toRunes.contains(ch) and
       idx != 0:
      return some idx
    elif ch == '\n'.Rune:
      return some idx
  return some(self.right.high)

func findAlpha(self: GapBuffer, skip: int = 0): Option[int] =
  for idx, ch in self.right[skip..^1]:
    if ch.isAlpha() or
       "1234567890".toRunes.contains(ch) and
       idx != 0:
      return some idx
  return  none int

func findBack(self: GapBuffer, needle: Rune, nth: int = 1): Option[int] =
  var found = 0
  for idx in countdown(self.left.high, 0):
    if self.left[idx] == needle and idx != self.left.high:
      result = some(self.left.len - idx)
      inc(found)
      if found == nth:
        return
    if self.left[idx] == '\n'.Rune:
      return none int

func findBackNonAlpha(self: GapBuffer, skip: int = 0): Option[int] =
  for idx in countdown(self.left.high - skip, 0):
    if not self.left[idx].isAlpha and
       not "1234567890".toRunes.contains(self.left[idx]) and
       idx != self.left.high: 
      return some(self.left.len - idx)
    if self.left[idx] == '\n'.Rune:
      return some(self.left.len - idx - skip)
  return some(self.left.len)

func findBackAlpha(self: GapBuffer, skip: int = 0): Option[int] =
  for idx in countdown(self.left.high - skip, 0):
    if self.left[idx].isAlpha or
       "1234567890".toRunes.contains(self.left[idx]) and
       idx != self.left.high: 
      return some(self.left.len - idx)
  return none int

proc lineDownTextObject*(self: GapBuffer): TextObject =
  let goalCol = self.cursor.col
  # move to linebreak
  let toMoveOpt = self.search('\n'.Rune)
  if toMoveOpt.isNone: 
    return
  let toMove = toMoveOpt.get()
  result.to = toMove + 1

  # now move to the column or to the end of line if the
  # line is shorter
  var next: Option[Rune]
  var col = 0
  while col < goalCol:
    next = self.peek(count = toMove + col + 1)
    if next.isNone() or next.get() == '\n'.Rune:
      return
    else:
      inc result.to
      inc col

proc lineDown*(self: var GapBuffer) =
  let textObj = self.lineDownTextObject()
  self.move textObj.to
      
proc lineUpTextObject*(self: GapBuffer): TextObject =
  let goalCol = self.cursor.col

  if self.searchback('\n'.Rune).isNone:
    return

  # move two linebreaks back to get to beginning of line
  let toMoveOpt = self.searchBack('\n'.Rune, 2)
  let toMove = toMoveOpt.get(otherwise = self.left.len)
  result.to = -toMove
  if toMoveOpt.isSome:
    inc result.to

  # now move to the colum or to the end of the line if the
  # line if shorter
  var next: Option[Rune]
  var col = 0
  let lineStart = abs(result.to)
  while col < goalCol:
    next = self.peekBack(lineStart - col - 1)
    if next.isNone or next.get == '\n'.Rune:
      return
    else:
      inc result.to
      inc col

proc lineUp*(self: var GapBuffer) =
  let textObj = self.lineUpTextObject()
  self.move textObj.to

proc gotoTextObject*(self: GapBuffer, ch: Rune): TextObject =
  let search = self.find(ch)
  if search.isNone:
    return
  else:
    result.to = search.get
    return

proc goto*(self: var GapBuffer, ch: Rune) =
  let textObj = self.goToTextObject(ch)
  self.move(textObj.to)

proc gotoBackTextObject*(self: GapBuffer, ch: Rune): TextObject =
  let next = self.peekBack
  if next.isSome and next.get == ch:
    result.frm = -1
    return
  let search = self.findBack(ch)
  if search.isNone:
    return
  else:
    result.frm = -search.get
    return

proc goToBack*(self: var GapBuffer, ch: Rune) =
  let textObj = self.goToBackTextObject(ch)
  self.move(textObj.frm)

proc lineStartTextObject*(self: GapBuffer): TextObject =
  let first = self.peekBack
  if first.isSome and first.get == '\n'.Rune:
    return
  let search = self.findBack('\n'.Rune)
  if search.isNone:
    result.to = self.left.len * -1
    return
  else:
    result.to = (search.get - 1) * -1
    return

proc goToLineStart*(self: var GapBuffer) =
  let textObj = self.lineStartTextObject()
  self.move(textObj.to)

proc lineEndTextObject*(self: GapBuffer): TextObject =
  let search = self.find('\n'.Rune)
  if search.isNone:
    result.to = self.right.len
    return
  else:
    result.to = search.get
    return

proc goToLineEnd*(self: var GapBuffer) =
  let textObj = self.lineEndTextObject()
  self.move(textObj.to)

proc wordForwardTextObject*(self: GapBuffer): TextObject =
  let searchNonAlpha = self.findNonAlpha()
  if searchNonAlpha.isNone:
    return
  else:
    let searchAlpha = self.findAlpha(skip = searchNonAlpha.get)
    if searchAlpha.isNone:
      return
    else:
      result.to = searchNonAlpha.get + searchAlpha.get

proc wordForward*(self: var GapBuffer) =
  let textObj = self.wordForwardTextObject
  if textObj.to != 0:
    self.move(textObj.to)

proc wordBackwardTextObject*(self: GapBuffer): TextObject =
  let searchAlphanum = self.findBackAlpha
  if searchAlphanum.isNone:
    return
  else:
    if not self.peekBack(searchAlphanum.get).get.isAlpha:
      result.to = -searchAlphanum.get
      return
    let searchNonAlpha = self.findBackNonAlpha(skip = searchAlphanum.get)
    if searchNonAlpha.isNone:
      return
    else:
      let steps = (searchAlphanum.get + searchNonAlpha.get)
  
      if steps >= self.left.len:
        result.to = -steps
      else:
        result.to = -searchNonAlpha.get + 1

proc wordBackward*(self: var GapBuffer) =
  let textObj = self.wordBackwardTextObject()
  self.move textObj.to

proc insert*(self: var GapBuffer, ch: Rune) =
  self.left.add ch
  if ch == '\n'.Rune:
    inc self.cursor.line
    self.cursor.col = 0
  else:
    inc self.cursor.col

proc delete*(self: var GapBuffer) =
  let deleted = self.left.pop
  if deleted == '\n'.Rune:
    dec self.cursor.line
    let goal = self.searchBack('\n'.Rune)
    if goal.isNone:
      self.cursor.col = len self.left
    else:
      self.cursor.col = goal.get - 1
  else:
    dec self.cursor.col

proc deleteForward*(self: var GapBuffer) =
  if self.right.len == 0:
    return
  discard self.right[0]
  self.right = self.right[1..^1]

proc innerWordTextObject*(self: GapBuffer): TextObject =
  let startOpt = self.findBackNonAlpha
  let endOpt = self.findNonAlpha

  let lastChar = self.peekBack()
  let insideWord = lastChar.isSome and lastChar.get.isAlpha
  if insideWord:
    result.frm = -startOpt.get(otherwise = self.left.len)
  if result.frm != -self.left.len and insideWord:
    inc result.frm
  result.to = endOpt.get(otherwise = self.right.len)
  if result.to == self.right.high:
    inc result.to

proc aroundWordTextObject*(self: GapBuffer): TextObject =
  result = self.innerWordTextObject
  let nextWordOpt = self.findAlpha(result.to)
  if nextWordOpt.isSome:
    result.to += nextWordOpt.get()
  else:
    result.to = len self.right

proc delete*(self: var GapBuffer, textObject: TextObject) =
  if textObject.frm != 0:
    self.left = self.left[0..self.left.high + textObject.frm]
  if textObject.to != 0:
    self.right = self.right[textObject.to..^1]
  self.cursor.col += textObject.frm

proc selectLine*(self: var GapBuffer) =
  let toStart = self.lineStartTextObject
  let toEnd = self.lineEndTextObject
  self.selection.frm = self.left.len + toStart.to
  self.selection.to = self.left.len + toEnd.to

proc resetSelection*(self: var GapBuffer) =
  self.selection.frm = 0
  self.selection.to = 0

iterator lines*(self: GapBuffer): string =
  for line in ($self).splitLines:
    yield line

iterator selected*(self: GapBuffer): Cursor =
  for idx in self.selection.frm..self.selection.to:
    yield self.idxToCursor(idx)
