# vin

An attempt at making a simple text editor.

This is mostly just a project for me to learn some things and to work
on an a bit bigger project than what I normally do.

For now at least I'm basing it on vim, so that it's easier to have some
a bit clearer goals to build towards, I'll probably go away from strictly
being the same but that will come with time. 

## Goals

The goals for this thing is just me learning, it being efficient, stable, and
anything like that is not at all a priority, learning, and having fun programming
are the only real goals, I also try to implement most of the things without
using many external libraries, I'm using illwill though, it's a pretty small abstraction
over the terminal system, but makes it so much better to work with, and it's a
pretty small dependency.

The gap-buffer that I'm using for this is also homegrown, and probably not
ideal, but I'm learning and it's fun. For now I'm basing it on vi, and we'll see if I can get it to work.

It also pretty much will mean that the documentation will not be very good, as I'm
just focusing on being able to make something that works rather than something good, if you're 
out for a text editor going for something actually good like (neo)vim or emacs is probably
the better way to go.

## Why nim

It's just the langauge I'm the most comfortable with, it's a really neat one, and is both performant
and safe, with a really nice type system. It's nice to work in a langauge that actually helps
me doing things better while being nice to look at, and not too in the way.

