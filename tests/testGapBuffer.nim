import std/unittest
import std/options
import std/unicode
import gapBuffer

suite "Gap Buffer":

  test "Create gapBuffer from string":
    let gb: GapBuffer = "Hello".toGapBuffer

    check gb.left == "".toRunes
    check gb.right == "Hello".toRunes

  test "Create empty gapBuffer":
    let gb: GapBuffer = newGapBuffer()

    check gb.left == "".toRunes
    check gb.right == "".toRunes

  test "Move forward":
    var gb = "Hello".toGapBuffer
    gb.move(2)

    check gb.left == "He".toRunes
    check gb.right == "llo".toRunes

  test "Move backwards":
    var gb = GapBuffer(left: "Hello".toRunes, right: @[])
    gb.move(-2)

    check gb.left == "Hel".toRunes
    check gb.right == "lo".toRunes

  test "Move second to last":
    var gb = GapBuffer(left: "H".toRunes, right: "ello".toRunes)
    gb.move(-1)

    check gb.left == "".toRunes
    check gb.right == "Hello".toRunes

  test "Lines iterator":
    let str = "abc\ndefg\nhij"
    let gb = str.toGapBuffer()
    var lines: seq[string] = @[]

    for line in gb.lines:
      lines.add line

    check lines == @["abc", "defg", "hij"]

  test "Peek success":
    let gb = "Hello".toGapBuffer()
    let pek = gb.peek()

    check pek.isSome()
    check pek.get() == 'H'.Rune

  test "Peek failure":
    var gb = "Hello".toGapbuffer()
    gb.move(5)
    let pek = gb.peek()

    check pek.isNone()

  test "PeekBack success":
    var gb = "Hello".toGapBuffer()
    gb.move(1)
    let pek = gb.peekBack()

    check pek.isSome()
    check pek.get() == 'H'.Rune

  test "PeekBack failure":
    let gb = "Hello".toGapBuffer()
    let pek = gb.peekBack()

    check pek.isNone()
    


suite "Gapbuffer movement":

  test "Char forward":
    var gb = "Hello".toGapbuffer()

    gb.charForward()

    check gb.left == "H".toRunes
    check gb.right == "ello".toRunes
    check gb.cursor.line == 0
    check gb.cursor.col == 1

  test "Char back":
    var gb = "Hello".toGapBuffer()

    gb.charForward()
    gb.charBackward()

    check gb.left == "".toRunes
    check gb.right == "Hello".toRunes
    check gb.cursor.line == 0
    check gb.cursor.col == 0

  test "Line down":
    var gb = "abc\ndefg\nhij".toGapBuffer()
    gb.charForward()
    gb.lineDown()

    check gb.left == "abc\nd".toRunes
    check gb.right == "efg\nhij".toRunes
    check gb.cursor.line == 1
    check gb.cursor.col == 1

  test "Line down shorter line":
    var gb = "abcdefg\nhi\njkl".toGapBuffer()
    gb.charForward()
    gb.charForward()
    gb.charForward()
    gb.charForward()
    gb.lineDown()

    check gb.left == "abcdefg\nhi".toRunes
    check gb.right == "\njkl".toRunes
    check gb.cursor.line == 1
    check gb.cursor.col == 2

  test "Line up":
    var gb = "abc\ndefg\nhij".toGapBuffer()
    gb.charForward()
    gb.lineDown()
    gb.lineUp()

    check gb.left == "a".toRunes
    check gb.right == "bc\ndefg\nhij".toRunes
    check gb.cursor.line == 0
    check gb.cursor.col == 1

  test "Line up shorter line":
    var gb = "ab\ncdef\nghi".toGapBuffer()
    gb.lineDown()
    gb.charForward()
    gb.charForward()
    gb.charForward()
    gb.lineUp()
    
    check gb.left == "ab".toRunes
    check gb.right == "\ncdef\nghi".toRunes
    check gb.cursor.line == 0
    check gb.cursor.col == 2

  test "Line up empty line":
    var gb = "abc\n\ndef".toGapBuffer()
    gb.lineDown()
    gb.lineDown()
    gb.charForward()
    gb.charForward()
    gb.lineUp()

    check gb.left == "abc\n".toRunes
    check gb.right == "\ndef".toRunes
    check gb.cursor.line == 1
    check gb.cursor.col == 0
  
  test "Line up double empty line":
    var gb = "abc\n\n\ndef".toGapBuffer()
    gb.lineDown()
    gb.lineDown()
    gb.lineDown()
    gb.charForward()
    gb.charForward()
    gb.lineUp()

    check gb.left == "abc\n\n".toRunes
    check gb.right == "\ndef".toRunes
    check gb.cursor.line == 2
    check gb.cursor.col == 0

  test "Line up first line":
    var gb = "test".toGapBuffer()
    gb.lineUp()

    check gb.left == "".toRunes
    check gb.right == "test".toRunes
    check gb.cursor.line == 0
    check gb.cursor.col == 0

  test "Insert char":
    var gb = newGapBuffer()

    gb.insert('H'.Rune)
    gb.insert('l'.Rune)
    gb.charBackward()
    gb.insert('e'.Rune)
    gb.charForward()
    gb.insert('l'.Rune)
    gb.insert('o'.Rune)

    check gb.left == "Hello".toRunes
    check gb.right == "".toRunes

  test "goto":
    var gb = "Hello".toGapBuffer()
    gb.goTo('l'.Rune)

    check gb.left == "He".toRunes
    check gb.right == "llo".toRunes
    check gb.cursor.line == 0
    check gb.cursor.col == 2

  test "goto when on equal char":
    var gb = "Hello".toGapBuffer()
    gb.goTo('l'.Rune)
    gb.goTo('l'.Rune)

    check gb.left == "Hel".toRunes
    check gb.right == "lo".toRunes
    check gb.cursor.line == 0
    check gb.cursor.col == 3

  test "gotoBack":
    var gb = "Hello".toGapBuffer()
    gb.charForward()
    gb.charForward()
    gb.charForward()
    gb.gotoBack('e'.Rune)

    check gb.left == "H".toRunes
    check gb.right == "ello".toRunes
    check gb.cursor.line == 0
    check gb.cursor.col == 1

  test "gotoLineEnd":
    var gb = "Hello".toGapBuffer()
    gb.gotoLineEnd()

    check gb.left == "Hello".toRunes
    check gb.right == "".toRunes
    check gb.cursor.line == 0
    check gb.cursor.col == 5

  test "gotoLineEnd multiple lines":
    var gb = "abc\ndef\nghi".toGapBuffer
    gb.lineDown
    gb.gotoLineEnd

    check gb.left == "abc\ndef".toRunes
    check gb.right == "\nghi".toRunes
    check gb.cursor.line == 1
    check gb.cursor.col == 3

  test "gotoLineStart":
    var gb = "Hello".toGapBuffer
    gb.gotoLineEnd
    gb.gotoLineStart

    check gb.left == "".toRunes
    check gb.right == "Hello".toRunes
    check gb.cursor.line == 0
    check gb.cursor.col == 0

  test "gotoLineEnd multiple lines":
    var gb = "abc\ndef\nghi".toGapBuffer
    gb.lineDown
    gb.gotoLineEnd
    gb.gotoLineStart

    check gb.left == "abc\n".toRunes
    check gb.right == "def\nghi".toRunes
    check gb.cursor.line == 1
    check gb.cursor.col == 0

  test "word forward":
    var gb = "This is a test".toGapBuffer
    gb.wordForward

    check gb.left == "This ".toRunes
    check gb.right == "is a test".toRunes
    check gb.cursor.line == 0
    check gb.cursor.col == 5

  test "word forward newline":
    var gb = "ab c\nde f".toGapBuffer
    gb.wordForward
    gb.wordForward
    
    check gb.left == "ab c\n".toRunes
    check gb.right == "de f".toRunes
    check gb.cursor.line == 1
    check gb.cursor.col == 0

  test "word backward":
    var gb = "this is a".toGapBuffer
    gb.wordForward
    gb.wordBackward

    check gb.left == "".toRunes
    check gb.right == "this is a".toRunes
    check gb.cursor.line == 0
    check gb.cursor.col == 0

  test "word backwards middle of line":
    var gb = "this is a".toGapBuffer
    gb.wordForward
    gb.wordForward
    gb.wordBackward
    
    check gb.left == "this ".toRunes
    check gb.right == "is a".toRunes
    check gb.cursor.line == 0
    check gb.cursor.col == 5

  test "word Backward newLine":
    var gb = "ab c\nde f".toGapBuffer
    gb.lineDown
    gb.wordBackward
    
    check gb.left == "ab ".toRunes
    check gb.right == "c\nde f".toRunes
    check gb.cursor.line == 0
    check gb.cursor.col == 3

  test "word backward basing":
    var gb = "what basing this".toGapBuffer
    gb.wordForward
    gb.wordForward
    gb.wordBackward

    check gb.left == "what ".toRunes
    check gb.right == "basing this".toRunes
    check gb.cursor.line == 0
    check gb.cursor.col == 5

  test "word backwards longer":
    var gb = "This is a line\nand here is another".toGapbuffer
    gb.lineDown
    gb.wordBackward

    check gb.left == "This is a ".toRunes
    check gb.right == "line\nand here is another".toRunes
    check gb.cursor.line == 0
    check gb.cursor.col == 10

  test "word backwards double newline":
    var gb = "This is a line\n\nand here is another".toGapbuffer
    gb.lineDown
    gb.lineDown
    gb.wordBackward

    check gb.left == "This is a ".toRunes
    check gb.right == "line\n\nand here is another".toRunes
    check gb.cursor.line == 0
    check gb.cursor.col == 10

suite "TextObject":
  test "gotoBack textObject":
    var gb = "with thousand rounds".toGapBuffer
    gb.wordForward
    gb.wordForward

    let goto = gb.gotoBackTextObject('s'.Rune)
    check goto.frm == -5
    check goto.to == 0

  test "gotoBack textObject next letter":
    var gb = "with thousand rounds".toGapBuffer
    gb.wordForward
    gb.wordForward
    gb.charBackward

    let goto = gb.gotoBackTextObject('d'.Rune)
    check goto.frm == -1
    check goto.to == 0

  test "inner word, middle of sentence":
    var gb = "With thousand rounds".toGapBuffer
    gb.wordForward
    gb.charForward
    gb.charForward

    let innerWord = gb.innerWordTextObject

    check innerWord.frm == -2
    check innerWord.to == 6

  test "inner word, middle of sentence, first character":
    var gb = "With thousand rounds".toGapBuffer
    gb.wordForward

    let innerWord = gb.innerWordTextObject

    check innerWord.frm == 0
    check innerWord.to == 8

  test "inner word, first word":
    var gb = "With thousand rounds".toGapBuffer
    gb.charForward
    gb.charForward

    let innerWord = gb.innerWordTextObject

    check innerWord.frm == -2
    check innerWord.to == 2

  test "inner word, last word":
    var gb = "With thousand rounds".toGapBuffer
    gb.wordForward
    gb.wordForward
    gb.charForward
    gb.charForward

    let innerWord = gb.innerWordTextObject

    check innerWord.frm == -2
    check innerWord.to == 4

  test "around word, middle of sentence":
    var gb = "With thousand rounds".toGapBuffer
    gb.wordForward
    gb.charForward
    gb.charForward

    let innerWord = gb.aroundWordTextObject

    check innerWord.frm == -2
    check innerWord.to == 7

  test "around word, first word":
    var gb = "With thousand rounds".toGapBuffer
    gb.charForward
    gb.charForward

    let innerWord = gb.aroundWordTextObject

    check innerWord.frm == -2
    check innerWord.to == 3

  test "around word, last word":
    var gb = "With thousand rounds".toGapBuffer
    gb.wordForward
    gb.wordForward
    gb.charForward
    gb.charForward

    let innerWord = gb.aroundWordTextObject

    check innerWord.frm == -2
    check innerWord.to == 4

  test "delete around word, middle of sentence":
    var gb = "With thousand rounds".toGapBuffer
    gb.wordForward
    gb.charForward
    gb.charForward

    let innerWord = gb.aroundWordTextObject
    gb.delete innerWord

    check gb.left == "With ".toRunes
    check gb.right == "rounds".toRunes
    check gb.cursor.col == 5

  test "delete around word, first word":
    var gb = "With thousand rounds".toGapBuffer
    gb.charForward
    gb.charForward

    let innerWord = gb.aroundWordTextObject
    gb.delete innerWord

    check gb.left == "".toRunes
    check gb.right == "thousand rounds".toRunes
    check gb.cursor.col == 0

suite "selection":
  test "select line":
    var gb = "abc\ndef\nghi".toGapBuffer
    gb.lineDown
    gb.selectLine
    
    check gb.selection.frm == 4
    check gb.selection.to == 7

  test "cursor iterator":
    var gb = "abc\ndef\nghi".toGapBuffer
    gb.lineDown
    gb.selectLine

    var cursors: seq[Cursor] = @[]
    for cursor in gb.selected:
      cursors.add cursor

    check cursors.len == 4
    for cursor in cursors:
      check cursor.line == 1
    check cursors[0].col == 0
    check cursors[1].col == 1
    check cursors[2].col == 2
    check cursors[3].col == 3

    
